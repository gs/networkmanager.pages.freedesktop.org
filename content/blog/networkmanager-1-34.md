+++
date = "2022-01-13T12:00:00+01:00"
title = "NetworkManager 1.34"
tags = ["release"]
draft = false
author = "Beniamino Galvani"
description = "What's new"
weight = 10
+++

Please welcome 1.34.0, the new major release of NetworkManager. Here
are some of the changes that it brings.

## More privilege separation

The new release introduces a new service named `nm-priv-helper`. At
the moment its functionality is very limited, but the idea is that
NetworkManager will perform every operation that requires elevated
privileges by asking the new service via D-Bus. The goal is to drop
more capabilities from the NetworkManager service.

## Wireguard support in nmtui

nmtui is a curses-based text-mode tool to conveniently configure
NetworkManager from a terminal. Thanks to the contribution of Javier
Sánchez Parra, nmtui now can be used to add and edit Wireguard
connection profiles.

![WireGuard in nmtui](/blog/images/nmtui-wg.png)

## API improvements

In the D-Bus API, the `Slaves` properties of the following interfaces:

- `org.freedesktop.NetworkManager.Device.Bond`
- `org.freedesktop.NetworkManager.Device.Bridge`
- `org.freedesktop.NetworkManager.Device.OvsBridge`
- `org.freedesktop.NetworkManager.Device.OvsPort`
- `org.freedesktop.NetworkManager.Device.Team`

are now deprecated. They are replaced by a new `Ports` property on the
`org.freedesktop.NetworkManager.Device` interface.

Similarly, these libnm functions:
- `nm_device_bond_get_slaves()`
- `nm_device_bridge_get_slaves()`
- `nm_device_team_get_slaves()`
- `nm_device_ovs_bridge_get_slaves()`
- `nm_device_ovs_port_get_slaves()`

are deprecated in favor of the new, generic `nm_device_get_ports()`.

## Other changes

The new release contains a number of other new features and
improvements, including:

 - support for configuring DNS over TLS (DoT) with systemd-resolved;

 - bond connections now support the `peer_notif_delay` option;
 
 - the `queue_id` option can now be set on bond ports to choose the TX
   queue id for each port;
 
 - nmcli accepts aliases `nmcli device up|down` in addition to `nmcli
   device connect|disconnect`;
 
 - the initrd generator now understands `ip=dhcp,dhcp6` to generate
   a connection that waits for both DHCPv4 and IPv6 autoconfiguration;
   
 - the initrd generator can parse kernel argument
   `rd.ethtool=INTERFACE:AUTONEG:SPEED` to configure autonegotiation
   and speed for an interface;

 - the initrd generator now understands the `ib.key` argument to
   create Infinibad P-Keys;

 - new configuration options (`allowed-connections` and
   `keep-configuration`) were added to provide a mechanism to activate
   after switch root different connections from those generated in the
   initramfs.

See the [full
changelog](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.34.0/NEWS)
for more information.

## Conclusions

The tarball for NM 1.34.0 is available
[here](https://download.gnome.org/sources/NetworkManager/1.34/NetworkManager-1.34.0.tar.xz).

This release was possible thanks to the contributions of: Ana Cabral,
Andrew Zaborowski, Beniamino Galvani, Benjamin Berg, Björn Lindqvist,
Christian Glombek, echengqi, Fernando Fernández Mancera, Frederic
Martinsons, gaoxingwang, Gris Ge, Harald van Dijk, Javier Jardón,
Javier Sánchez Parra, Jonas Kümmerlin, josef radinger, Julian
Wiedmann, Lubomir Rintel, Lukasz Majewski, Maxine Aubrey, Michael
Catanzaro, Nacho Barrientos, Philip Withnall, Rain-lk, Robin Ebert,
Sibo Dong, Simon McVittie, Thomas Haller, Vojtech Bubela, wangce, Wen
Liang, xiangnian, Yu Watanabe.
