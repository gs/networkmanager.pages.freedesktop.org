+++
date = "2020-12-06T12:00:00+01:00"
title = "NetworkManager 1.28 [link]"
tags = ["release"]
draft = false
author = "Thomas Haller"
description = "What's new"
weight = 10
+++

A new NetworkManager version 1.28.0 was released today, on 6th December 2020.

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2020/12/06/networkmanager-1-28-0/).
