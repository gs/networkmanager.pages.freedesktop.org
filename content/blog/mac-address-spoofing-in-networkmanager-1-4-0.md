+++
date = "2016-08-26T12:00:00+01:00"
title = "MAC Address Spoofing in NetworkManager 1.4.0 [link]"
tags = ["mac"]
draft = false
author = "Thomas Haller"
description = "Change the current MAC address of your Ethernet or Wi-Fi card"
weight = 10
+++

The new NetworkManager release 1.4.0 adds new features to change the
current MAC address of your Ethernet or Wi-Fi card. This is also
called MAC address “spoofing” or “cloning”.

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2016/08/26/mac-address-spoofing-in-networkmanager-1-4-0/).
