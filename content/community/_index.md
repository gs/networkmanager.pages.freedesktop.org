+++
draft= false
title = "Community"
description = "How to get in touch with the NetworkManager community."
+++

### Repository and issue tracker

https://gitlab.freedesktop.org/NetworkManager/NetworkManager

### Mailing list

https://mail.gnome.org/mailman/listinfo/networkmanager-list

### Matrix

[`#networkmanager:matrix.org`](https://matrix.to/#/#networkmanager:matrix.org)

### IRC

#nm on [Libera.Chat](https://libera.chat/)
