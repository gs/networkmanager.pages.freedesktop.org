#!/bin/bash

BASE_URL="https://download.gnome.org/sources/NetworkManager"

BASE_VERS=( $(wget -q -O - "$BASE_URL" | sed -n 's/.*<a \+href="\([0-9]\+\.[0-9]\+\)\/".*/\1/p') )

log_stderr() {
    printf "%s\n" "$*" >&2
}

V=()
for v in "${BASE_VERS[@]}" ; do
    U="$BASE_URL/$v"
    log_stderr ">>base $U"
    FILES=( $(wget -q -O - "$U" | sed -n 's/.*<a \+href="\(NetworkManager-[^"]\+\)".*/\1/p') )
    for f in "${FILES[@]}" ; do
        log_stderr ">>>>file $U/$f"
    done
    V+=( $(printf '%s\n' "${FILES[@]}" | sed -n 's/^NetworkManager-\(.*\)\.tar\.[a-z0-9]\+$/\1/p' | sort -u) )
done

printf "%s\n" "${V[@]}" | sort --version-sort
